#import dependencies
from bioblend import galaxy
import time, argparse, requests, os, csv
import psycopg2

#import Classes
from user import User
from instance import Instance
from database import Database

#{u'username': u'dchristiany', u'deleted': False, u'id': u'a799d38679e985db', u'last_password_change': u'2018-05-17T15:07:08.015458', u'active': True, u'model_class': u'User', u'email': u'david.christiany@gmail.com'}

def get_args() :
    parser = argparse.ArgumentParser()
    parser.add_argument("--url", help="url of galaxy instance to copy", required=True)
    parser.add_argument("-k","--key", help="admin apikey of instance to copy", required=True)
    parser.add_argument("-h1","--host",help="",default=None )
    parser.add_argument("-u","--user",help="",default=None)
    parser.add_argument("-db","--database",help="",default=None)
    parser.add_argument("-p","--password",help="",default=None)
    parser.add_argument("--tmp_dir",help="",default='.')
    args = parser.parse_args()
    if args.url[-1]!='/' : args.url = args.url+"/"
    return args

def create_users_infos_file(users,path,bdd_access):
    path = os.path.join(path,"users_infos.tsv")
    if bdd_access:
        header = ["user","id","username","apikey","nb_histories","nb_total_histories","histories_list","last history update","password","disk_usage","nb_workflows","workflows_ids","workflows_names","created","last connection"]
    else:
        header = ["user","id","username","apikey","nb_histories","nb_total_histories","histories_list","last history update","disk_usage","nb_workflows","workflows_ids","workflows_names"]    
    with open(path, 'w') as output:
        writer = csv.writer(output,delimiter="\t")
        writer.writerow(header)
        for user in users:
            histories_list = ";".join(user.histories)
            if bdd_access:
                line = [user.mail,user.id,user.name,user.apikey,user.nb_histories,user.nb_total_histories,histories_list,user.last_history_update,user.pwd,user.disk_usage,user.nb_workflows,";".join(user.workflows_id),";".join(user.workflows_names),user.creation_date,user.last_connection]
            else:
                line = [user.mail,user.id,user.name,user.apikey,user.nb_histories,user.nb_total_histories,histories_list,user.last_history_update,user.disk_usage,user.nb_workflows,";".join(user.workflows_id),";".join(user.workflows_names)]
            writer.writerow(line)

def main():

    #get args from command
    print ("get args")
    args = get_args()
    bdd_access = args.host is not None and args.user is not None and args.database is not None and args.password is not None
    if bdd_access is False:
        print ('no psql database infos')

    print ("init instance: "+str(args.url))
    #init galaxy instances
    galaxy_ref = Instance(args.url,args.key)

    if bdd_access:
        print ("init database: "+str(args.database))
        #init database instances
        bdd_ref = Database(user = args.user, password = args.password, host = args.host, database = args.database)

    print ("galaxy_ref version: "+str(galaxy_ref.version))

    #get users from ref galaxy
    print ("getting users")
    users = galaxy_ref.get_users()

    #get missing attributes
    print("getting users infos")
    for user in users:

        #get attributes
        user.histories = galaxy_ref.get_histories_ids(user.apikey)
        user.total_histories = galaxy_ref.get_histories_ids(user.apikey,deleted=True)
        user.last_history_update = galaxy_ref.get_last_history_update(user)
        user.workflows = galaxy_ref.get_workflows(user)
        
        #if psql bdd access
        if bdd_access:
            user.pwd = bdd_ref.get_password(user)
            user.creation_date = bdd_ref.get_creation_date(user)
            user.last_connection = bdd_ref.get_last_connection(user)

    print ("creating users infos file")
    create_users_infos_file(users,args.tmp_dir,bdd_access)

if __name__ == "__main__":
    # execute only if run as a script
    main()
