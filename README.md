# Transfert galaxy user accounts

    transfert_galaxy_users.py 

## What it does

* get users profiles from galaxy instance
* create users profiles on a target instance if needed, with the same email, username and password 
* get workflows
* download users histories
* save histories on local folder
* upload histories to target instance
* upload workflows to target Galaxy instance

## Beforehand
### Create conda env

    conda create -n galaxy_transfer python=3.7.3 #or above
    python -m pip install git+https://github.com/galaxyproject/bioblend
    conda install psycopg2

### Activate conda env

    conda activate galaxy_transfer

### Required informations

* **URL** and **admin apikey** for Galaxy instance to copy and target Galaxy instance
* login for psql databases linked to Galaxy instance:
 - **host**
 - **user**
 - **database_name**
 - **password** 

## Usage

Scripts must be run in galaxy_scripts folder (instance.py, user.py and database.py needed to load class)

Example:

    python transfert_galaxy_users.py --ref <Galaxy_URL> --key <admin_apikey>
                                     --target <Galaxy_target_URL>  --target_key <admin_apikey> 
                                     --host1 <host1> -u1 <user1> -db1 <database_name1> -p1 <password1> 
                                     --host2 <host2> -u2 <user2> -db2 <database_name2> -p2 <password2>
                                     --tmp_dir histories/

Connect two psql database at the same time can be troublesome, that's why we added the --save-passwords option.
You can save passwords in a file and upload it on the target instance afterwards.
Saved passwords are crypted, created account on the target instance have a random password.

Example: 

    python transfert_galaxy_users.py --ref <Galaxy_URL> --key <admin_apikey>
                                     --target <Galaxy_target_URL>  --target_key <admin_apikey> 
                                     --host1 <host1> -u1 <user1> -db1 <database_name1> -p1 <password1> 
                                     --save-passwords True 
                                     --save_passwords_path <path>/passwords.txt

You can now use the passswords file to update passwords on the target instance.
To upload passwords you just need the psql database, the Galaxy API is not involved.

    python update_passwords.py  --host <host2> -u <user2> -db <database2> -p <password2> -f passwords.txt

You can also transfered account without keeping the same passwords for users, in that case random passwords will be attributed.
Users will have to recreate a password with the link 'Click here to reset your password' at the Galaxy login page.

Example:

    python transfert_galaxy_users.py --ref <Galaxy_URL> --key <admin_apikey>
                                     --target <Galaxy_target_URL>  --target_key <admin_apikey> 