#import dependencies
from bioblend import galaxy
import time, argparse, requests, os, csv

#import Classes
from user import User
from instance import Instance

def get_args() :
    parser = argparse.ArgumentParser()
    parser.add_argument("--url", help="url of galaxy instance", required=True)
    parser.add_argument("-k","--key", help="apikey of user", required=True)
    parser.add_argument("--tmp_dir",help="",default='galaxy_histories')
    parser.add_argument("--skip_pending_histories", help="Boolean, if true only download history archives already available", type=bool, default=False, required=False)
    parser.add_argument('--user_mails', nargs='+', help='Mails of users to transfer, not transferring other users', required=False, default=None)
    args = parser.parse_args()
    if args.url[-1]!='/' : args.url = args.url+"/"
    return args

def main():
    
    #get args from command
    print ("get args")
    args = get_args()

    print ("init instance: "+str(args.url))
    #init galaxy instances
    galaxy_ref = Instance(args.url,args.key)
    print ("galaxy version: "+str(galaxy_ref.version))

    #get users from ref galaxy
    print ("getting users")
    users = galaxy_ref.get_users(args.user_mails)

    #create tmp_dir if needed
    os.makedirs(args.tmp_dir, exist_ok=True)

    #download histories:
    print("download histories")
    nb_users = len(users)
    for i,user in enumerate(users):
        print ("user "+str(i)+"/"+str(nb_users))
        print ("downloading "+str(user.name)+" histories:")
        histories_path = args.tmp_dir+"/"+user.name+"_"+user.id+"_histories"
        user.histories = galaxy_ref.get_histories_ids(user.apikey)
        galaxy_ref.download_histories(user.histories,histories_path,args.skip_pending_histories)

if __name__ == "__main__":
    main()