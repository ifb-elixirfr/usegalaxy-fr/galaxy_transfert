#import dependencies
from bioblend import galaxy
import time, argparse, requests, os, csv, shutil, pickle, tarfile
import psycopg2

#import Classes
from user import User
from instance import Instance
from database import Database

#{u'username': u'dchristiany', u'deleted': False, u'id': u'a799d38679e985db', u'last_password_change': u'2018-05-17T15:07:08.015458', u'active': True, u'model_class': u'User', u'email': u'david.christiany@gmail.com'}

def get_args() :
    parser = argparse.ArgumentParser()
    parser.add_argument("--ref", help="url of galaxy instance to copy", required=True)
    parser.add_argument("--target", help="url of galaxy instance to target", required=True)
    parser.add_argument("-k1","--key", help="admin apikey of instance to copy", required=True)
    parser.add_argument("-k2","--target_key", help="admin apikey of instance to target", required=True)
    parser.add_argument("-h1","--host1",help="")
    parser.add_argument("-h2","--host2",help="")
    parser.add_argument("-u1","--user1",help="")
    parser.add_argument("-u2","--user2",help="")
    parser.add_argument("-db1","--database1",help="")
    parser.add_argument("-db2","--database2",help="")
    parser.add_argument("-p1","--password1",help="")
    parser.add_argument("-p2","--password2",help="")
    parser.add_argument("--tmp_dir",help="",default='galaxy_histories')
    parser.add_argument("--skip_pending_histories", help="Boolean, if true: only download history archives already available", type=bool, default=False)
    parser.add_argument("--keep_histories", help="Keep downloaded histories after upload on the target Galaxy instance", type=bool, default=False)
    parser.add_argument("--from_scratch", help="All users will be transfer, even if some where already transfered", type=bool, default=False)
    parser.add_argument("--save_passwords", help="Create a file with all crypted passwords, this file is used to update user's password on another instance", type=bool, default=False)
    parser.add_argument("--save_passwords_path", help="File path to save passwords file")
    parser.add_argument("--transfer_passwords", help="Passwords retrieved from the reference Galaxy instance are transfered to the target instance (need psql database connection)", type=bool, default=True)
    parser.add_argument('--user_mails', nargs='+', help='Mails of users to transfer, not transferring other users', required=False, default=None)
    parser.add_argument('--all_galaxy_version', help="By default only Galaxy from version 19.01 are allowed, this option allows all Galaxy version", required=False, type=bool, default=False) 
    args = parser.parse_args()
    if args.ref[-1]!='/' : args.ref = args.ref+"/"
    if args.target[-1]!='/' : args.target = args.target+"/"
    return args

def check_galaxies_versions(ref,target):
    if ref.version < 19.01:
        raise Exception('reference Galaxy instance version needs to be at least 19.01 (to export histories')
    if target.version < 20.01 : 
        raise Exception('target Galaxy instance version needs to be at least 20.01 (to upload histories)') 
    if target.version < ref.version:
        raise Exception('target Galaxy instance version can not be inferior to reference Galaxy instance')
    return (target.version >= ref.version) and (ref.version >= 19.01) and (target.version >= 20.01)

def init_transfered_users_list():
    if os.path.exists('.transfered_users'):
        with open('.transfered_users','rb') as handler:
            transfered_users = pickle.load(handler)
    else : 
        transfered_users = []
    return transfered_users

def check_tarfile_integrity(tar_path):
    if os.path.exists(tar_path):
        try:
            tar = tarfile.open(tar_path)
        except tarfile.ReadError as e:
            print (e)
            return False
        try:
            members = tar.getmembers()
        except IOError as e:
            print (e)
            return False
        except EOFError as e:
            print (e)
            return False
        return True
    else:
        print (tar_path,"does not exist")
        return False

def main():
    #get args from command
    #print ("get args")
    args = get_args()
    bdd_access = args.host1 is not None and args.user1 is not None and args.database1 is not None and args.password1 is not None
    bdd_target_access = args.host2 is not None and args.user2 is not None and args.database2 is not None and args.password2 is not None
    if bdd_access is False:
        print ('no psql database infos, passwords can not be retrieved or transfered')

    #print ("init instances")
    #init galaxy instances
    galaxy_ref = Instance(args.ref,args.key)
    print ('reference Galaxy instance: ' + galaxy_ref.url)
    galaxy_target = Instance(args.target,args.target_key)
    print ('target Galaxy instance: '+ galaxy_target.url)
    
    #init database instances
    if bdd_access:
        bdd_ref = Database(user = args.user1, password = args.password1, host = args.host1, database = args.database1)
        print ('reference psql database: '+bdd_ref.database)
    if bdd_target_access:
        bdd_target = Database(user = args.user2, password = args.password2, host = args.host2, database = args.database2)
        print ('target psql database: '+bdd_target.database)

    #check if target galaxy version is high enough
    if args.all_galaxy_version or check_galaxies_versions(galaxy_ref,galaxy_target):
        print ("reference Galaxy version: "+str(galaxy_ref.version))
        print ("target Galaxy version: "+str(galaxy_target.version))

        #get users from ref galaxy and target galaxy
        print ("Getting users")
        users = galaxy_ref.get_users(args.user_mails)

        #save password file path
        if  args.save_passwords:
            if args.save_passwords_path:
                save_pass_file = args.save_passwords_path
                if save_pass_file[-1] == '/' : save_pass_file += 'galaxy_passwords.txt'
            else:
                save_pass_file = "galaxy_passwords.txt"
            pass_file = open(save_pass_file,"a+")

        #create temporary file to save progression
        if args.from_scratch:
            transfered_users = []
        else:
            transfered_users = init_transfered_users_list()
            print (str(len(transfered_users))+' users already transfered')

        #create tmp_dir if needed
        os.makedirs(args.tmp_dir, exist_ok=True)

        #remove already transfered users
        users = [user for user in users if user.id not in transfered_users]

        #get missing attributes
        for n_user,user in enumerate(users):
            histories_path = args.tmp_dir+"/"+user.name+"_"+user.id+"_histories"
            print ("\nUser "+str(n_user+1)+"/"+str(len(users))+":\t"+user.mail)
            #get attributes
            user.histories = galaxy_ref.get_histories_ids(user.apikey)
            user.total_histories = galaxy_ref.get_histories_ids(user.apikey,deleted=True)
            user.workflows_infos = galaxy_ref.get_workflows(user)
            if bdd_access:
                user.pwd = bdd_ref.get_password(user)
                user.creation_date = bdd_ref.get_creation_date(user)
                user.last_connection = bdd_ref.get_last_connection(user)

            #create user on target instance
            if user.is_user(galaxy_target):
                print (user.mail+" exists on target instance: "+galaxy_target.url)
                user.target_id = galaxy_target.get_id(user)
                user.target_key = galaxy_target.get_apikey(user.target_id)
                if user.target_key == 'Not available.' : user.target_key = galaxy_target.create_apikey(user.target_id)
            else :
                print ("creating user on target instance")
                galaxy_target.create_user(user)
                if args.transfer_passwords and bdd_target_access and user.pwd is not None:
                    bdd_target.update_password(user.mail, user.pwd)    #set the same password as the one in galaxy_ref

            #check histories
            #nb_queued = len([id for id in user.histories if galaxy_ref.get_history_state(id) == 'queued'])
            #user.histories = [id for id in user.histories if galaxy_ref.get_history_state(id) != 'queued']
            #print (nb_queued, "histories in queued status, these histories will not be downloaded" )

            #remove empty history 
            nb_empty = len([id for id in user.histories if galaxy_ref.is_history_empty(id)])
            user.histories = [id for id in user.histories if galaxy_ref.is_history_empty(id) is False]
            print (nb_empty, "empty histories removed from histories list to download")

            #download histories
            print (user.nb_histories,"histories found for",user.mail)
            print ("\nDownloading histories")
            galaxy_ref.download_histories(user.histories,histories_path,args.skip_pending_histories)

            #check tarfiles integrity 
            print("\nChecking tar.gz file(s) integrity...")
            tar_files = [ os.path.join(histories_path,tar_file) for tar_file in os.listdir(histories_path)]
            corrupted_tar = [tar_path for tar_path in tar_files if check_tarfile_integrity(tar_path) is False]
            corrupted_ids = [tar_path.split('/')[-1].replace('.tar.gz','') for tar_path in corrupted_tar]
            corrupted_ids = [ id for id in corrupted_ids if id in user.histories]
            if len(corrupted_tar ) > 0:
                print ('Some tar.gz has not been properly downloaded and are corrupted')
                print ("\n".join(corrupted_tar))
                ntry = 1
                while len(corrupted_tar ) > 0 and ntry < 3:
                    print ("\nRemoving "+str(len(corrupted_tar))+" corrupted tar.gz file(s)")
                    for tar in corrupted_tar:
                        os.remove(tar)
                        print (tar,"deleted")
                    print ("\nDownloading again")
                    galaxy_ref.download_histories(corrupted_ids,histories_path,args.skip_pending_histories)
                    corrupted_tar = [tar_path for tar_path in corrupted_tar if check_tarfile_integrity(tar_path) is False]
                    ntry+=1
            if len(corrupted_tar) == 0 : print("All histories have been properly downloaded")
            else: print ("After 3 attempts some histories are still not properly downloaded")

            #upload histories
            print('\nUploading '+user.name+' histories from '+histories_path+' to ' + galaxy_target.url)
            galaxy_target.upload_histories(user.target_key,histories_path)

            if args.keep_histories is not True:
                print("\nDeleting "+histories_path)
                shutil.rmtree(histories_path)

            #remove 'imported from archive prefix'
            uploaded_histories = galaxy_target.get_histories_ids(user.target_key)
            for id in uploaded_histories:
                hist_name = galaxy_target.get_history_name(id)
                if 'imported from archive: ' in hist_name:
                    hist_name = hist_name.replace('imported from archive: ','')
                    galaxy_target.update_history_name(id,hist_name)

            #download workflows
            print("\nDownloading workflows")
            user.workflows = galaxy_ref.download_workflows(user.workflows_id)
            print(str(user.nb_workflows)+' workflows retrieved')
            #print("Done\n")

            #upload workflows
            if user.nb_workflows > 0:
                print("\nupload workflows")
                galaxy_target.import_workflows(user.workflows,user.target_key)
                #print("Done\n")
            
            #Save password
            if bdd_access and args.save_passwords:
                print("\nsaving password") 
                pass_file.write('\t'.join([user.mail,user.pwd])+"\n")

            #Add transfered user to list
            print(user.mail+' transfered to '+ galaxy_target.url)
            transfered_users.append(user.id)

            #Save progression
            with open('.transfered_users','wb') as handler:
                pickle.dump(transfered_users,handler)

        #close pass_file
        if args.save_passwords:
            pass_file.close()

        #remove tmp_file
        print("\nremoving temporary file")
        os.remove('.transfered_users')
        print('Done')

    else :
        print ("bad galaxy version")
        exit()

if __name__ == "__main__":
    # execute only if run as a script
    main()
