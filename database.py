#import dependencies
from bioblend import galaxy
import time, argparse, requests, os, csv
import psycopg2

class Database:

    def __init__(self,user,password,host,database):
        self.user = user
        self.password = password
        self.host = host
        self.database = database

    def get_password(self,user):
        try:
            connection = psycopg2.connect(user=self.user, password=self.password, host=self.host, database=self.database)
            cursor = connection.cursor()
            query = """select password from galaxy_user where email = %s;"""
            cursor.execute(query,(user.mail,))
            return cursor.fetchone()[0]
        except (Exception, psycopg2.Error) as error :
            print ("Error while connecting to PostgreSQL", error)
        finally:
            if(connection):
                cursor.close()
                connection.close()

    def update_password(self,mail,password):
        try:
            connection = psycopg2.connect(user=self.user, password=self.password, host=self.host, database=self.database)
            cursor = connection.cursor()
            sql_update_query = """update galaxy_user set password = %s where email = %s;"""
            cursor.execute(sql_update_query,(password,mail))       #update password
            connection.commit()
        except (Exception, psycopg2.Error) as error :
            print ("Error while connecting to PostgreSQL", error)
        finally:
                if(connection):
                    cursor.close()
                    connection.close()

    def get_passwords(self):
        try:
            connection = psycopg2.connect(user=self.user, password=self.password, host=self.host, database=self.database)
            cursor = connection.cursor()
            cursor.execute("select email,password from galaxy_user;")
            record = cursor.fetchall()
            return record
        except (Exception, psycopg2.Error) as error :
            print ("Error while connecting to PostgreSQL", error)
        finally:
            if(connection):
                cursor.close()
                connection.close()
            
    def get_creation_date(self,user):
        try:
            connection = psycopg2.connect(user=self.user, password=self.password, host=self.host, database=self.database)
            cursor = connection.cursor()
            command=str("select create_time from galaxy_user where email='"+user.mail+"';")
            cursor.execute(command)
            record = cursor.fetchall()
            return (record[0][0]) #datetime object
        except (Exception, psycopg2.Error) as error :
            print ("Error while connecting to PostgreSQL", error)
        finally:
            #closing database connection.
                if(connection):
                    cursor.close()
                    connection.close()
                    #print("PostgreSQL connection is closed")

    def get_last_connection(self,user):
        try:
            connection = psycopg2.connect(user=self.user, password=self.password, host=self.host, database=self.database)
            cursor = connection.cursor()
            command=str("select galaxy_session.update_time from galaxy_session join galaxy_user on galaxy_session.user_id = galaxy_user.id where galaxy_user.email='"+user.mail+"' order by galaxy_session.update_time DESC limit 1;")
            cursor.execute(command)
            record = cursor.fetchall()
            return (record[0][0]) #datetime object
        except (Exception, psycopg2.Error) as error :
            print ("Error while connecting to PostgreSQL for last connection of user "+user.name, error)
        finally:
            #closing database connection.
                if(connection):
                    cursor.close()
                    connection.close()
                    #print("PostgreSQL connection is closed")

def remove_ascii_bad_characters(text):
    return ''.join([i if ord(i) < 128 else ' ' for i in text])

def create_users_infos_file(users,path):
    path = os.path.join(path,"users_infos.csv")
    header = ["user","id","username","apikey","nb_histories","nb_total_histories","histories_list","password","disk_usage","nb_workflows","workflows_ids","workflows_names","created","last connection"]
    with open(path, 'w') as output:
        writer = csv.writer(output,delimiter="\t")
        writer.writerow(header)
        for user in users:
            line = [user.mail,user.id,user.name,user.apikey,user.nb_histories,user.nb_total_histories,user.histories,user.pwd,user.disk_usage,user.nb_workflows,";".join(user.workflows_id),";".join(user.workflows_names),user.creation_date,user.last_connection]
            writer.writerow(line)
