#import dependencies
from bioblend import galaxy
import time, argparse, requests, os, csv

def remove_ascii_bad_characters(text):
    return ''.join([i if ord(i) < 128 else ' ' for i in text])

class User:

    def __init__(self,mail,id,username):
        self.mail = mail
        self.id = id
        self.apikey = None
        self.name = username
        self.pwd = None
        self.histories = None
        self.total_histories = None
        self.quota = None
        self.disk_usage = None
        self.target_id = None
        self.target_key = None
        self.workflows = None
        self.workflows_infos = None
        self.creation_date = None
        self.last_connection = None
        self.last_history_update = None

    def is_user(self,instance):
        return self.mail in instance.mails

    @property
    def workflows_id(self):
        if self.workflows_infos is not None:
            return [wf['id'] for wf in self.workflows_infos]

    @property
    def workflows_names(self):
        if self.workflows_infos is not None:
            return [remove_ascii_bad_characters(wf['name']) for wf in self.workflows_infos]

    @property
    def nb_workflows(self):
        if self.workflows_infos is not None:
            return len(self.workflows_infos)

    @property
    def nb_histories(self):
        if self.histories is not None:
            return len(self.histories)

    @property
    def nb_total_histories(self):
        if self.total_histories is not None:
            return len(self.histories)
