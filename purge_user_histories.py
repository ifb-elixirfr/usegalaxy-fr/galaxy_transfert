#import dependencies
from bioblend import galaxy
import time, argparse, requests, os, csv

#import Classes
from user import User
from instance import Instance

def get_args() :
    parser = argparse.ArgumentParser()
    parser.add_argument("--url", help="url of galaxy instance", required=True)
    parser.add_argument("-k","--key", help="apikey of user", required=True)
    parser.add_argument('--user_mails', nargs='+', help='Mails of users to transfer, not transferring other users', required=True, default=None)
    parser.add_argument('-p','--purge', help="Delete and purge histories (remove files)", type=bool, required=False, default=False)
    args = parser.parse_args()
    if args.url[-1]!='/' : args.url = args.url+"/"
    return args

def main():
    
    #get args from command
    print ("get args")
    args = get_args()

    print ("init instance: "+str(args.url))
    #init galaxy instances
    galaxy_ref = Instance(args.url,args.key)
    print ("galaxy version: "+str(galaxy_ref.version))

    #get users from ref galaxy
    print ("getting users")
    users = galaxy_ref.get_users(args.user_mails)

    #remove histories:
    nb_users = len(users)
    for i,user in enumerate(users):
        print ("user "+str(i)+"/"+str(nb_users))
        user.histories = galaxy_ref.get_histories_ids(user.apikey)
        print (len(user.histories),'found for ', user.mail)
        print ("purge histories is", args.purge)
        galaxy_ref.delete_histories(args.key,user.histories,purge=args.purge)

if __name__ == "__main__":
    main()