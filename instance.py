#import dependencies
from bioblend import galaxy
from bioblend import ConnectionError 
import time, argparse, requests, os, csv
import string, random, requests

#import Classes
from user import User

class Instance:

    def __init__(self,url,admin_key):
        self.url = url
        self.admin_key = admin_key
        self.workflows = galaxy.GalaxyInstance(url,admin_key).workflows
        self.config = galaxy.GalaxyInstance(url,admin_key).config
        self.usr = galaxy.GalaxyInstance(url,admin_key).users
        #self.quotas = galaxy.GalaxyInstance(url,admin_key).quotas

    @property
    def mails(self):
        return [e['email'] for e in self.usr.get_users()]

    @property
    def version(self):
        return float(self.config.get_version()['version_major'])

    @property
    def gi(self):
        return galaxy.GalaxyInstance(url=self.url, key=self.admin_key)

    def get_user(self,mail):
        tmp_user = self.usr.get_users(f_email=mail)[0]
        user = User(tmp_user['email'],tmp_user['id'],tmp_user['username'])
        user.apikey = self.get_apikey(user.id)
        if user.apikey == 'Not available.' : user.apikey = self.create_apikey(user.id)
        user.disk_usage = self.usr.show_user(user.id)['nice_total_disk_usage']
        user.quota = self.usr.show_user(user.id)['quota']
        return user

    def get_users(self, mails=None):
        if mails:
            return [self.get_user(mail) for mail in mails]
        else:
            users_list = self.usr.get_users()
            users=[]
            for element in users_list:
                user = User(element['email'],element['id'],element['username'])
                user.apikey = self.get_apikey(user.id)
                if user.apikey == 'Not available.' : user.apikey = self.create_apikey(user.id)
                user.disk_usage = self.usr.show_user(user.id)['nice_total_disk_usage']
                user.quota = self.usr.show_user(user.id)['quota']
                users.append(user)
            return users

    def get_workflows(self,user):
        return galaxy.GalaxyInstance(url=self.url,key=user.apikey).workflows.get_workflows()

    def download_workflow(self,id):
        try :
            return self.workflows.export_workflow_dict(id)
        except:
            pass

    def download_workflows(self, ids):
        wfs = [self.download_workflow(id) for id in ids]
        wfs = [wf for wf in wfs if wf is not None]
        return wfs

    def create_user(self,user):
        password = create_random_password()
        try:
            response = self.usr.create_local_user(user.name,user.mail,password)
        except ConnectionError as e :
            if '400008' in e.body:
                print ("username already exist for another user, adding '_usegalaxyfr' to username ")
                username = user.name+"_usegalaxyfr"
                response = self.usr.create_local_user(username,user.mail,password)
            else:
                raise(e)
        user.target_id = response['id']
        user.target_key =  self.create_apikey(user.target_id)
        self.usr = galaxy.GalaxyInstance(self.url,self.admin_key).users             #update usr

    def get_id(self,user):
        for usr in self.usr.get_users(): 
            if usr['email'] == user.mail: 
                return usr['id']
        return None

    def get_apikey(self,id):
        return self.usr.get_user_apikey(id)

    def create_apikey(self,id):
        return self.usr.create_user_apikey(id)

    def get_histories_ids(self,key,deleted=False):
        gi = galaxy.GalaxyInstance(url=self.url, key=key)
        histories = galaxy.histories.HistoryClient(gi)
        return  [history['id'] for history in histories.get_histories(deleted=deleted)]

    def get_last_history_update(self,user):
        gi = galaxy.GalaxyInstance(url=self.url, key=user.apikey)
        histories = galaxy.histories.HistoryClient(gi)
        ids = [history['id'] for history in histories.get_histories()]
        if len(ids) > 0:
            update_times = sorted([histories.show_history(id)['update_time'] for id in ids],reverse=True)
            return update_times[0]
        else :
            return ''

    #return dictionary
    def get_history_infos(self,id):
        return self.gi.histories.show_history(id)

    def get_history_state(self,id):
        return self.gi.histories.get_status(id)['state']

    def get_history_name(self,id):
        return self.gi.histories.show_history(id)['name']

    def update_history_name(self,id,history_name):
        self.gi.histories.update_history(id,name=history_name)

    def get_history_size(self,id):
        return self.gi.histories.show_history(id)['size']

    #return boolean
    def is_history_empty(self,id):
        return self.gi.histories.show_history(id)['empty']

    def init_history_archives(self,histories,histories_list):
        pending_histories = []
        for id in histories_list:
            jeha_id=histories.export_history(id, gzip=True, include_hidden=False, include_deleted=False, wait=False, maxwait = 5) #maxwait in seconds
            if jeha_id == "":
                pending_histories.append(id)
        return pending_histories

    def download_histories(self,histories_list,path,skip):
        if not os.path.exists(path): os.mkdir(path)
        gi = galaxy.GalaxyInstance(url=self.url, key=self.admin_key)
        histories = galaxy.histories.HistoryClient(gi)
        pending_histories = self.init_history_archives(histories,histories_list)                         #init histories archives
        i=1
        done_histories = []
        while len(done_histories) < len(histories_list):
            for id in histories_list:
                if id not in pending_histories and id not in done_histories:
                    filename = path+"/"+id+".tar.gz"
                    print (str(i)+"/"+str(len(histories_list))+"\t"+filename+"\t"+str(self.get_history_size(id))+"\t"+self.get_history_name(id))
                    if not os.path.exists(filename):
                        with open(filename,"wb") as outf:  
                            for j in range(5):
                                try:
                                    jeha_id=histories.export_history(id, gzip=True, include_hidden=False, include_deleted=False, wait=False, maxwait = 5)
                                    histories.download_history(id, jeha_id, outf, chunk_size=4096)
                                    done_histories.append(id)
                                except requests.exceptions.HTTPError as e:
                                    if j < 5 :
                                        print (e)
                                        time.sleep(0.5)
                                        continue
                                    else:
                                        print ("5 attempt without success for "+id)
                                        raise
                                break
                    else:
                        done_histories.append(id)
                    i+=1
            if skip is True :
                print (str(i-1)+"/"+str(len(histories_list))+" histories downloaded")
                break
            else :
                if len(done_histories) < len(histories_list):
                    print ("waiting for pending histories (1')")
                    print ("\n".join(pending_histories))
                    time.sleep(60)
                #update histories status
                pending_histories = self.init_history_archives(histories,pending_histories)

    def upload_history(self,key,path):
        if os.path.exists(path) and os.path.isfile(path):
            gi = galaxy.GalaxyInstance(url=self.url, key=key)
            histories = galaxy.histories.HistoryClient(gi)
            try :
                histories.import_history(file_path=path)
            except requests.exceptions.ConnectionError as e:
                print (e)
                print ("upload of this history is still ongoing" )
        else:
            return "file path not valid"

    def upload_histories(self,key,path):
        if os.path.exists(path):
            gi = galaxy.GalaxyInstance(url=self.url, key=key)
            histories = galaxy.histories.HistoryClient(gi)
            if os.path.isdir(path):
                files = os.listdir(path)
                for i,file in enumerate(files):
                    print (str(i+1)+"/"+str(len(files))+"\t"+str(file))
                    try :
                        histories.import_history(file_path=os.path.join(path,file))
                    except requests.exceptions.ConnectionError as e:
                        print(e)
                        print ("upload of this history is still ongoing" )
                    except:
                        print ("upload failed for", file)
                    time.sleep(5)
            else:
                print("histories path is not a directory as expected")
                return histories.import_history(file_path=path)
        else:
            return "file path not valid"

    def delete_history(self,key,hist_id,purge=False):
        gi = galaxy.GalaxyInstance(url=self.url, key=key)
        histories = galaxy.histories.HistoryClient(gi)
        print ('removing history ',hist_id)
        histories.delete_history(hist_id,purge=purge)

    def delete_histories(self,key,id_list,purge=False):
        gi = galaxy.GalaxyInstance(url=self.url, key=key)
        histories = galaxy.histories.HistoryClient(gi)       
        for hist_id in id_list:
            print ('removing history ',hist_id)
            histories.delete_history(hist_id,purge=purge)

    @property
    def quotas(self):
        quotas_list = self.quotas.get_quotas()
        return [self.quotas.show_quota(q['id']) for q in quotas_list]

    def create_quota(self):
        pass

    def import_workflows(self,workflows,key):
        gi = galaxy.GalaxyInstance(url=self.url, key=key)
        workflowsClient = galaxy.workflows.WorkflowClient(gi)
        for i,wf in enumerate(workflows):
            print (str(i+1)+"/"+str(len(workflows))+"\t"+wf['name'])
            try:
                workflowsClient.import_workflow_dict(wf,publish=False)
            except requests.exceptions.HTTPError as e:
                print(e)

def create_random_password(stringLength=10):
    password_characters = string.ascii_letters + string.digits + string.punctuation
    return ''.join(random.choice(password_characters) for i in range(stringLength))