#import dependencies
from bioblend import galaxy
import time, argparse, requests, os, csv

#import Classes
from user import User
from instance import Instance

def get_args() :
    parser = argparse.ArgumentParser()
    parser.add_argument("--url", help="url of galaxy instance", required=True)
    parser.add_argument("-k","--key", help="apikey of user", required=True)
    parser.add_argument("--histories_path",help="hitories directory or history to upload",required=False)
    parser.add_argument("--history",help="hitories directory or history to upload",required=False)
    args = parser.parse_args()
    if args.url[-1]!='/' : args.url = args.url+"/"
    return args

def main():
    
    #get args from command
    print ("get args")
    args = get_args()

    print ("init instance: "+str(args.url))
    #init galaxy instances
    galaxy_target = Instance(args.url,args.key)
    print ("galaxy version: "+str(galaxy_target.version))

    #upload history
    if args.history and os.path.isfile(args.history):
        print('Uploading '+args.history+' to ' + galaxy_target.url)
        galaxy_target.upload_history(args.key,args.history)

    #upload histories
    if args.histories_path and os.path.isdir(args.histories_path):
        print('Uploading  histories from '+args.histories_path+' to ' + galaxy_target.url)
        galaxy_target.upload_histories(args.key,args.histories_path)

    #remove 'imported from archive prefix'
    histories = galaxy_target.get_histories_ids(args.key)
    for id in histories:
        hist_name = galaxy_target.get_history_name(id)
        if 'imported from archive: ' in hist_name:
            hist_name.strip('imported from archive: ')
            galaxy_target.update_history_name(id,hist_name)

if __name__ == "__main__":
    main()